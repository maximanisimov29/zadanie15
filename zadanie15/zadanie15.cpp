#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#pragma comment(lib, "user32.lib")
using namespace std;
void _tmain(intargc, _TCHAR* argv[])



{
	SYSTEM_INFO siSysInfo;

	// Копируем информацию о железе в структуру SYSTEM_INFO.

	GetSystemInfo(&siSysInfo);

	// Отображаем содержимое структуры SYSTEM_INFO.

	printf("Hardware information: \n");
	printf("  OEM ID: %u\n", siSysInfo.dwOemId);
	printf("  Number of processors: %u\n", siSysInfo.dwNumberOfProcessors);
	printf("  Page size: %u\n", siSysInfo.dwPageSize);
	printf("  Processor type: %u\n", siSysInfo.dwProcessorType);
	printf("  Minimum application address: %lx\n", siSysInfo.lpMinimumApplicationAddress);
	printf("  Maximum application address: %lx\n", siSysInfo.lpMaximumApplicationAddress);
	printf("  Active processor mask: %u\n", siSysInfo.dwActiveProcessorMask);

}
